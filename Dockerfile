FROM ddarshana/alpinenode10

ENV NODE_ENV=production
RUN apk add --update curl && rm -rf /var/cache/apk/*
RUN mkdir /app
WORKDIR /app


RUN npm install
COPY . .

CMD ["npm", "start"]